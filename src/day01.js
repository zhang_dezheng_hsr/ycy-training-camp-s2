import * as THREE from 'three';
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader';

// 场景
const scene = new THREE.Scene()

// 相机
/**
 * 广角【摄像机视锥体垂直视野角度】： 75
 * 宽高比【摄像机视锥体长宽比】： window.innerWidth / window.innerHeight
 * 近采点面【摄像机视锥体近端面】: 0.01
 * 远采点面【摄像机视锥体近端面】：10
 * 渲染范围/视野范围：0.01 - 10
 */
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.01, 10)


// 渲染器 WebGL1Renderer
const renderer = new THREE.WebGL1Renderer({ 
  // 抗锯齿
  antialias: true
})
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)

// three没对这个做类型检查：0,0,2
// 传字符串，会报错
camera.position.set(0,0,2)

// 鼠标控制3d物体的类
const controls = new OrbitControls(camera, renderer.domElement)

//  创建环境光，让所有东西都亮起来。可以加一点点，让暗处不会那么暗，看起来不会那么假
// const ambientLight = new THREE.AmbientLight(0xffffff, 0.2)
// scene.add(ambientLight)

// 方向光 让物体亮起了的方式之一
// const directionLight = new THREE.DirectionalLight(0xffffff, 1)
// scene.add(directionLight)

// 加背景色 让物体亮起了的方式之一
// const directionLight = new THREE.DirectionalLight(0xffffff, 1)
// scene.background = new THREE.Color(0.6,0.6,)

// // 物体  创建立方体：长宽高均为 1
// const boxGeometry = new THREE.BoxGeometry(1, 1, 1)

// // 材质  MeshBasicMaterial:基础材质
// const boxMaterial = new THREE.MeshBasicMaterial({ color: 0x00ff00})

let mixer
// // 网格 Mesh
// const boxMesh = new THREE.Mesh(boxGeometry, boxMaterial) // scene.add(boxMesh)

// import donuts.glb 
new GLTFLoader().load('../resources/models/donuts.glb', gltf => {

  // traverse 是 导出的数据自带的遍历数组的方法
  // gltf.scene.traverse(child => {
  //   // child.receiveShadow = true
  //   // child.castShadow = true
  //   console.log(child)
  // })

  // 缺少会看不到物体
  scene.add(gltf.scene)
  
  // 当场景中的多个对象独立动画时，每个对象都可以使用同一个动画混合器。
  mixer = new THREE.AnimationMixer(gltf.scene)
  // play each animation
  const clips = gltf.animations 

  clips.forEach(clip => {
    const action = mixer.clipAction(clip)
    // 只播放一次
    action.loop = THREE.LoopOnce
    console.log('firs********t', action)
    action.play()
    // 动画停在最后一帧
    action.clampWhenFinished = true
  })
})

// 使用方向光可以照亮环境。
// 让物体亮起了的方式之一
// 但是有个技巧：pdr材质建议使用hdr照亮环境，这样金属材质不会显得很黑
// blender默认材质是pdr
new RGBELoader()
  .load('../resources/sky.hdr', (texture) => {
    texture.mapping = THREE.EquirectangularReflectionMapping
    scene.environment = texture
    renderer.outputEncoding = THREE.sRGBEncoding
    renderer.render(scene, camera)
  })
  
// 帧循环
function animation() {
  requestAnimationFrame(animation)
  renderer.render(scene, camera)
  controls.update()
// 0.02 必须填否则没有数据,必填项
  mixer?.update(0.02)
}

// 动画入口
animation()