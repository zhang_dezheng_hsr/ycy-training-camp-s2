import * as THREE from 'three';
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader';

// 场景
const scene = new THREE.Scene()

// 相机
/**
 * 广角【摄像机视锥体垂直视野角度】： 75
 * 宽高比【摄像机视锥体长宽比】： window.innerWidth / window.innerHeight
 * 近采点面【摄像机视锥体近端面】: 0.01
 * 远采点面【摄像机视锥体近端面】：10
 * 渲染范围/视野范围：0.01 - 10
 */
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.01, 10)


// 渲染器 WebGL1Renderer
const renderer = new THREE.WebGL1Renderer({ 
  // 抗锯齿
  antialias: true
})
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)

// three没对这个做类型检查：0,0,2
// 传字符串，会报错
camera.position.set(0,0,2)

// 鼠标控制3d物体的类
const controls = new OrbitControls(camera, renderer.domElement)

//  创建环境光，让所有东西都亮起来
const ambientLight = new THREE.AmbientLight(0xffffff, 0.5)
scene.add(ambientLight)

// 物体  创建立方体：长宽高均为 1
const boxGeometry = new THREE.BoxGeometry(1, 1, 1)

// 材质  MeshBasicMaterial:基础材质
const boxMaterial = new THREE.MeshBasicMaterial({ color: 0x00ff00})

// 网格 Mesh
const boxMesh = new THREE.Mesh(boxGeometry, boxMaterial)
scene.add(boxMesh)

// 帧循环
function animation() {
  requestAnimationFrame(animation)
  renderer.render(scene, camera)
  controls.update()
}

// 动画入口
animation()